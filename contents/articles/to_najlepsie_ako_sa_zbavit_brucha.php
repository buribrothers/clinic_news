<?php
$title = "TO NAJLEPŠIE Ako sa zbaviť brucha";
$picture = "http://somjedinecomam.sk/wp-content/uploads/2014/04/Brucho-5a-660x393.jpg";
$posted = "apríl 30th, 2014 by admin";
$description = "Problém s ovísajúcou a nadbytočnou kožou na bruchu majú mnohí z nás. Tí, ktorí vyhľadávajú pomoc plastických chirurgov, aby sa zbavili nadbytočnej kože na bruchu, vyhľadávajú expertov na – ABDOMINOPLASTIKU.";

if (isset($_GET['section'])) {
    createArticleDescription($title, $picture, $posted, $description, "to_najlepsie_ako_sa_zbavit_brucha");
} else {
    ?>
    <div id="page" class="post">
        <h1 class="post-title">TO NAJLEPŠIE Ako sa zbaviť brucha</h1>
        <div class="content"><div class="box-content"><img width="660" height="393" src="http://somjedinecomam.sk/wp-content/uploads/2014/04/Brucho-5a-660x393.jpg" class="attachment-col4 wp-post-image" alt="brucho-tlste-ilustr"></div></div>        

        <div class="postmetadata">
            <hr>
            <div class="post-date"><strong>Posted on</strong>: apríl 30th, 2014			 by 
                admin</div>
            <hr>
        </div>

        <div class="entry">
            <p><i>Problém s ovísajúcou a nadbytočnou kožou na bruchu majú mnohí z nás. Tí, ktorí vyhľadávajú pomoc plastických chirurgov, aby sa zbavili nadbytočnej kože na bruchu, vyhľadávajú expertov na – ABDOMINOPLASTIKU.&nbsp;</i><em><span id="more-4010"></span></em></p>
            <p>Najčastejšími pacientmi sú mamičky po pôrode, ľudia, ktorí zhodili veľkú časť zo svojej váhy alebo jedinci, <strong>ktorým sa nedarí&nbsp;</strong> diétou ani cvičením zhodiť tuk a nadbytočnú kožu z brucha dole.</p>
            <p><a href="http://www.interklinik.sk/abdominoplastika" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">Abdominoplastika</a> je vážnejší chirurgický zákrok pri ktorom sa <strong>odstraňuje nadbytok kože a&nbsp;tuku</strong> zo strednej a&nbsp;dolnej časti brucha, napínajú sa svaly brušnej steny.</p>
            <p>&nbsp;</p>
            <p><img title="pred-po" alt="" src="http://somjedinecomam.sk/wp-content/uploads/2011/04/pred-po.jpg" width="426" height="339"><img title="Čítajte ďalej..." alt="" src="http://somjedinecomam.sk/wp-includes/js/tinymce/plugins/wordpress/img/trans.gif"></p>
            <p>&nbsp;</p>
            <p>Operácia môže trvať od 2 do 5 hodín. Fajčiari by mali prestať fajčiť aspoň 1 týždeň pred zákrokom a 2 týždne po zákroku, nakoľko fajčenie spomaľuje hojenie a zvyšuje riziko možných komplikácií.</p>
            <p><a href="http://www.interklinik.sk/abdominoplastika" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">Zákrok sa robí</a> pod&nbsp;<strong>celkovou anestézou</strong>, počas ktorej budete pekne spať. Po operácií by ste sa mali snažiť čo najrýchlejšie chodiť a cvičiť. Samozrejme len tak veľa, kým sa budete cítiť komfortne.&nbsp;Chodenie a cvičenie vám pomôže k rýchlejšiemu hojeniu a približne o 2-4 týždne sa môžete vrátiť do práce.</p>
            <p><strong>Výsledok je dlhodobý</strong>, &nbsp;jediné na čo sa musíte pripraviť je <strong>trvalá jazva</strong>. Ale nová postava stojí za to!</p>
            <p>VIDEO Ako sa robí abdominoplastika (<em>Tommy Tuck</em>) – ilustrácia. (angl)</p>
            <p><iframe src="//www.youtube.com/embed/ET96fd7ess8" height="480" width="640" allowfullscreen="" frameborder="0"></iframe></p>
            <p>Zdroj: interklinik, plasticka chirurgia;</p>
            <p><strong>&nbsp;Prečítajte si tiež:</strong>&nbsp;<a href="http://www.interklinik.sk/abdominoplastika" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">Abdominoplastika</a>; <a href="http://somjedinecomam.sk/2014/01/to-najlepsie-lipofiling-vlastnym-tukom/" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">To najlepšie-Lipofiling</a>;<a href="http://somjedinecomam.sk/2014/02/odstranenie-mandli/" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false"><br>
                </a></p>
            <p><b>Túžite po pestrejších informáciách?</b></p>
            <p>Spojte sa s nami:</p>
            <ul>
                <li>na&nbsp;<a href="http://www.interklinik.sk/" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">Interklinik.sk</a><b>&nbsp;</b>nájdete všetko o nás</li>
                <li>na<a href="http://www.facebook.com/pages/Interklinik-centrum-zdravia-a-krasy/182030630479" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">&nbsp;Facebooku</a>&nbsp;je všetko pre fanúšikov</li>
                <li>na&nbsp;<a href="http://www.flickr.com/photos/sijedinecomas/" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">Flickr&nbsp;</a>na vás prehovoria naše fotky</li>
                <li>na&nbsp;<a href="http://www.linkedin.com/company/interklinik" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">LindkedIn</a>&nbsp;získajte ďalšie informácie</li>
            </ul>
        </div>
    </div>
    <?php
}
?>