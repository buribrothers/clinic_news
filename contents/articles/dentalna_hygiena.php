<?php
$title = "Dentálna hygiena alebo poďme sa správne starať o naše zuby";
$picture = "http://somjedinecomam.sk/wp-content/uploads/2016/03/DentalHygieneMouth-660x440.jpg";
$posted = "marec 15th, 2016 by admin";
$description = "Určite to poznáte – „Osvedčené triky pre biely úsmev za pár dní“ alebo „Krásne zuby z&nbsp;pohodlia domova“ – časté titulky s&nbsp;bielením chrupu v&nbsp;hlavnej úlohe. Ak trápi odtieň vašich zubov aj vás, prípadne ste niekedy zvažovali jeho vylepšenie alebo ste ho dokonca podstúpili bez očakávaného výsledku, sú tieto riadky určené priamo vám. Faktom totiž je, že bielemu úsmevu sa venujeme čím ďalej tým viac pozornosti.";

if (isset($_GET['section'])) {
    createArticleDescription($title, $picture, $posted, $description, "dentalna_hygiena");
} else {
    ?>
    <div id="page" class="post">
        <h1 class="post-title">Dentálna hygiena alebo poďme sa správne starať o naše zuby</h1>
        <div class="content"><div class="box-content"><img width="660" height="440" src="http://somjedinecomam.sk/wp-content/uploads/2016/03/DentalHygieneMouth-660x440.jpg" class="attachment-col4 wp-post-image" alt="dental_interklinik"></div></div>        

        <div class="postmetadata">
            <hr>
            <div class="post-date"><strong>Posted on</strong>: marec 15th, 2016			 by 
                admin</div>
            <hr>
        </div>

        <div class="entry">
            <p><i>Je niekoľko dôvodov, prečo by ste starostlivosť o&nbsp;svoju ústnu dutinu nemali nechávať len na starú známu zubnú kefku. Tá sa síce dobre postará o&nbsp;ľahšie prístupné plochy vašich zubov, ale pre vyčistenie tých náročnejších miest je potrebná pomoc ďalšich pomocníkov, ktoré sú dnes bežne dostupné v&nbsp;každej lekárni, drogérii či supermarkete. Ako však sa však o&nbsp;naše zuby správať nielen často, viacerými spôsobmi, ale aj správne?</i><span id="more-5769"></span></p>
            <p><strong>Škola základ života</strong></p>
            <p>Zaiste si mnohí z&nbsp;vás spomenú na časy, kedy sme „drali“ lavice základnej školy a&nbsp;k povinnej výbave mladého školáka patrili nielen nezabudnuteľné prezuvky, ale aj nemenej populárne hygienické vrecúško. To nám rodičia vybavovali mydlom, uteráčikom či zubnou kefkou a&nbsp;pastou. Každý deň po obede mala spiatočná cesta z&nbsp;jedálne do tried ešte jednu povinnú medzizastávku, a&nbsp;to umývadlá, kde sme sa so spolužiakmi pretekali o „najlepšiu pozíciu“ pri čistení zubov. Možno sme si to teda ani nestihli uvedomiť a&nbsp;potreba starostlivosti o&nbsp;naše zuby nám bola vštepovaná už od útleho veku.</p>
            <p><strong>Každý sme originál, aj v&nbsp;čistení zubov</strong></p>
            <p>Na svete však neexistujú dvaja ľudia s&nbsp;rovnakým „rituálom“ čistenia zubov. Keď si však chceme osvojiť tú správnu techniku, je dôležité si uvedomiť, že koľko ľudí, toľko chrupov a&nbsp;koľko chrupov, toľko rôznych zubov. Každý z&nbsp;nich má iný tvar, veľkosť či zafarbenie. Aj preto by ste si mali okrem pravidelnej zubnej prehliadky nájsť aspoň raz za rok priestor pre návštevu odborníka na starostlivosť o&nbsp;chrup, čiže dentálnu hygienu. U&nbsp;nás v&nbsp;Interklinik je práve takým odborníkom Alenka Černá. Kráse a&nbsp;zdraviu zubov sa venuje už 15 rokov a&nbsp;s&nbsp;presvedčením hovorí, že „krásne zuby nehradí ani dokonalý make-up, ani drahé oblečenie“.</p>
            <p><strong>Správna technika je dôležitá</strong></p>
            <p>Alena Černá tiež zdôrazňuje, že pravidelné čistenie zubov je dôležité, avšak je to práve správna technika čistenia, za ktorú vás vaše zuby odmenia zdravou ústnou dutinou, sviežim dychom a pekným úsmevom. V&nbsp;jej ambulancii nenájdete len pomoc v&nbsp;prípade súviciacich problémov, ale naučíte sa im aj predchádzať. Oboznámi vás so správnou starostlivosťou o zuby, najnovšími pomôckami na dentálnu hygienu a&nbsp;naučí vás ich správne používať.</p>
            <p><strong>Ako to funguje..</strong></p>
            <p>Základným liečebným postupom v&nbsp;kresle dentálnej hygieničky je intenzívne odstraňovanie zubného kameňa a&nbsp;povlakov najčastejšie použitím ultrazvuku. Následne sú zuby pieskované práškom, pričom sa vyčistia medzizubné priestory a&nbsp;rôzne pigmentové sfarbenia. Takéto dôkladné ošetrenie je zároveň prevenciou pred zubným kazom, zápalom ďasien, parodontózou a sfarbovaním zubov.&nbsp;Je tiež príležitosťou odhaliť akékoľvek poškodenia skloviny, pričom lekár povrch skloviny profesionálne vyleští.</p>
            <p><strong>Dohodnite si termín</strong></p>
            <p>A&nbsp;kedy je už skutočne najvyšší čas zveriť sa do rúk dentálnej hygieničky? Pomyselná „kontrolka“ by sa vám mala rozblikať pri problémoch s&nbsp;ďasnami (krvácaní, opuchoch, bolestiach, zmene tvaru), zapáchajúcom dychu, kývajúcich sa zuboch či zubnom kameni. Vo všeobecnosti by však mal ambulanciu odborníka navštíviť každý, komu na zdraví svojich zubov záleží. Nikto by nemal zabúdať na to, že prevencia je menej náročná ako liečba, a&nbsp;to po každej stránke. A&nbsp;ak by niekto na tento fakt niekedy zabudol, radi mu to s&nbsp;našou dentálnou hygieničkou pripomenieme.</p>
        </div>
    </div>
    <?php
}
?>