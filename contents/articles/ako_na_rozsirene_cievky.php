<?php
$title = "Ako na rozšírené cievky";
$picture = "http://somjedinecomam.sk/wp-content/uploads/2014/05/Tvár-koža-krém-1.jpg";
$posted = "máj 5th, 2014 by admin";
$description = "Viditeľné cievky na tvári alebo na stehnách ľudia často mylne považujú za popraskané. V&nbsp;skutočnosti ide o&nbsp;úplne iný problém – o&nbsp;stratu elasticity. Zdravé cievy sa prirodzene rozširujú a&nbsp;sťahujú… Odpovedá primárka dermatológie Interklinik – MUDr. SLAVOMÍRA FEIX";

if (isset($_GET['section'])) {
    createArticleDescription($title, $picture, $posted, $description, "ako_na_rozsirene_cievky");
} else {
    ?>
    <div id="page" class="post">
        <h1 class="post-title">Ako na rozšírené cievky</h1>
        <div class="content"><div class="box-content"><img width="448" height="525" src="http://somjedinecomam.sk/wp-content/uploads/2014/05/Tvár-koža-krém-1.jpg" class="attachment-col4 wp-post-image" alt="Tvar-koza-krém-ilustr"></div></div>        

        <div class="postmetadata">
            <hr>
    <!--                <strong>Category</strong>: <a href="http://somjedinecomam.sk/category/anti-aging/" rel="category tag" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">Anti-aging</a>, <a href="http://somjedinecomam.sk/category/estetika/" rel="category tag" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">Estetika</a>            
                    <div class="tags"><strong>Tags</strong>: <a href="http://somjedinecomam.sk/tag/dermatologia-a-interklinik/" rel="tag" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">dermatológia a interklinik</a>, <a href="http://somjedinecomam.sk/tag/dermatologia-a-mudr-slavomira-feix/" rel="tag" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">dermatologia a MUDr.Slavomira Feix</a>, <a href="http://somjedinecomam.sk/tag/dermatologia-a-rozsirene-cievky/" rel="tag" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">dermatologia a rozsirene cievky</a> </div>-->

            <div class="post-date"><strong>Posted on</strong>: máj 5th, 2014			 by 
                admin </div>
            <hr>
        </div>

        <div class="entry">
            <p><i>Viditeľné cievky na tvári alebo na stehnách ľudia často mylne považujú za popraskané. V&nbsp;skutočnosti ide o&nbsp;úplne iný problém – o&nbsp;stratu elasticity. Zdravé cievy sa prirodzene rozširujú a&nbsp;sťahujú… Odpovedá primárka dermatológie Interklinik – MUDr. SLAVOMÍRA FEIX</i><span id="more-4041"></span></p>
            <p>Takýmto spôsobom cievy reagujú na zmenu teploty. Ak stratia túto schopnosť, postupne sa rozširujú bez toho, aby sa opätovne stiahli, a&nbsp;tak sa do nich nahrnie viac krvi. Preto sú čoraz viditeľnejšie. Odborne sa tomuto problému hovorí <i>kupertóza</i>. Najčastejšie rozšírenými cievkami trpia ľudia so suchou citlivou pleťou.</p>
            <p><b>Aká je fyziognomická príčina rozšírených cievok? Zohráva pri ich vzniku hlavnú úlohu genetika alebo životný štýl?</b></p>
            <p>Pri rozšírených cievkach na tvári ide o rozšírenie drobnokalibrových cievok v hornej vrstve kože. Majú genetický podklad, ľudia, ktorí na ne trpia, sa často už rodia s takzvanými pavúčikovitými znamienkami, ale môžu sa prejaviť aj sekundárne, ako sprievodný znak iného ochorenia. Ide napríklad o poruchy pečene, krvné ochorenia, cievne a kožné. Objavujú sa však aj pod vplyvom vonkajších podnetov, napríklad vysokej teploty. Preto ich mávajú na tvári pracovníci pri horúcich peciach alebo ľudia, ktorí chodievajú do sauny a často vystavujú pokožku slnečnému žiareniu. Dá sa teda povedať, že na tvorbe rozšírených cievok sa môžu podieľať genetika aj životný štýl.</p>
            <p><b>Je to len estetický alebo aj zdravotný problém?</b></p>
            <p>Všetko, čo je viditeľné na koži a vymyká sa z normálu, vyhodnocujeme ako estetický problém, ale ako som už spomenula, v prípade rozšírených cievok môže ísť aj o ukazovateľ iného ochorenia. Ak sa nejaké cievky vytvoria alebo vytvárajú počas života, treba navštíviť lekára a vylúčiť iné, závažnejšie ochorenia.</p>
            <p><b>Aké sú možnosti riešenia tohto problému?</b></p>
            <p>Vždy sa snažíme v medicíne riešiť príčinu, vtedy zmiznú aj sprievodné príznaky ochorenia. Ak nepoznáme príčinu alebo sa nepreukáže žiadna spojitosť s iným vážnym ochorením, môžeme tento estetický rušivý jav stlmiť laserovou medicínou. Tá vytlačila doteraz agresívnejšiu prácu elektrickou ihlou, po ktorej často ostávali jazvičky. Na tvári pri <i>rozacei</i>, plameni alebo angiómoch používame jemný argonový alebo KTP-laser, ktorý jemne cievky zlepšuje, po ošetrení sa stávajú neviditeľnými. Pri výraznejších a hlbších cievnych rozšíreniach je vynikajúci <a href="http://www.interklinik.sk/laserova-medicina" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">NEODYM YAG laser</a>. Využíva sa hlavne pri drobných kŕčových žilách a metličkách na nohách.– dodáva dermatologička MUDr. Slavomíra Feix.</p>
            <p><b>Čo sa dá robiť, ak po zásahu laserom ostanú cievky naďalej viditeľné?</b></p>
            <p>Výsledok ošetrenia závisí od energie, s ktorou sa pracovalo. Možno ju zvýšiť alebo predĺžiť čas expozície, alebo vykonávať ošetrenia postupne, na viackrát.</p>
            <p><img class="alignleft size-full wp-image-4054" alt="cievky-rozsirene-ilustr" src="http://somjedinecomam.sk/wp-content/uploads/2014/05/Cievky-rozšírené-kuperóza-1.jpg" width="554" height="299"></p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p><b>Možnosti prevencie</b></p>
            <p>Najlepšou prevenciou je <b>predchádzať základnému ochoreniu.</b> Napríklad pri metličkách na nohách sú dôležité zásady <a href="http://somjedinecomam.sk/2013/12/zily-krcove-varix/" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">prevencie kŕčových žíl,</a> pri rozšírených cievkach na tvári by sme nemali vystavovať postihnuté miesta<b> slnku a teplu.</b> Treba sa vyhýbať dlhému <b>saunovaniu a používaniu infračervených lámp</b>, odporúča dermatologička <a href="http://www.interklinik.sk/mudr-feix" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">MUDr.Slavomíra Feix.</a></p>
            <ul>
                <li>Vyhýbajte sa <b>kozmetickým prípravkom</b>, ako sú niektoré typy peelingov, pleťových vôd a masiek, ktoré majú prekrvujúce účinky. Vhodné nie je ani <b>umývanie horúcou vodou</b></li>
                <li>chráňte svoju pleť<b> </b>pre nežiaducim<b> UV žiarením</b> ochrannými krémami</li>
                <li>užívajte <b>vitamín C a vitamíny</b> na posilnenie cievnych stien.</li>
                <li>vyhýbajte sa extrémnym zmenám teploty a obmedzte spotrebu <b>alkoholu a kávy.</b></li>
                <li><b>prestaňte fajčiť.</b></li>
            </ul>
            <p>&nbsp;</p>
            <p>Viac si o rozšírených cievkach prečítajte v&nbsp;<a href="http://zena.sme.sk/c/7192204/ako-na-rozsirene-cievky.html" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">SME žena</a> č.17/2014</p>
            <p>&nbsp;</p>
            <p><b>Prečítajte si tiež:</b>&nbsp;<a href="http://somjedinecomam.sk/2014/02/koza-starnutie-dermatolog/" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">Keď koža starne</a>&nbsp;;&nbsp;<a href="http://somjedinecomam.sk/2014/01/laser-osetrenie-tvare/" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">Laserove ošetrenie tváre</a>;</p>
            <p><b>Túžite po pestrejších informáciách?</b></p>
            <p>Spojte sa s nami:</p>
            <ul>
                <li>na&nbsp;<a href="http://www.interklinik.sk/" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">Interklinik.sk</a><b>&nbsp;</b>nájdete všetko o nás</li>
                <li>na<a href="http://www.facebook.com/pages/Interklinik-centrum-zdravia-a-krasy/182030630479" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">&nbsp;Facebooku</a>&nbsp;je všetko pre fanúšikov</li>
                <li>na&nbsp;<a href="http://www.flickr.com/photos/sijedinecomas/" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">Flickr&nbsp;</a>na vás prehovoria naše fotky</li>
                <li>na&nbsp;<a href="http://www.linkedin.com/company/interklinik" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">LindkedIn</a>&nbsp;získajte ďalšie informácie</li>
            </ul>
        </div>
    </div>
    <?php
}
?>
