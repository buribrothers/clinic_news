<?php
$title = "Bielim, bieliš, bielime. Jednoducho a doma";
$picture = "http://somjedinecomam.sk/wp-content/uploads/2016/05/Teeth-Whitening-New-York-660x256.jpg";
$posted = "máj 16th, 2016 by admin";
$description = "Určite to poznáte – „Osvedčené triky pre biely úsmev za pár dní“ alebo „Krásne zuby z&nbsp;pohodlia domova“ – časté titulky s&nbsp;bielením chrupu v&nbsp;hlavnej úlohe. Ak trápi odtieň vašich zubov aj vás, prípadne ste niekedy zvažovali jeho vylepšenie alebo ste ho dokonca podstúpili bez očakávaného výsledku, sú tieto riadky určené priamo vám. Faktom totiž je, že bielemu úsmevu sa venujeme čím ďalej tým viac pozornosti.";

if (isset($_GET['section'])) {
    createArticleDescription($title, $picture, $posted, $description, "bielim_bielis_bielime");
} else {
    ?>
    <div id="page" class="post">        
        <h1 class="post-title">Bielim, bieliš, bielime. Jednoducho a doma</h1>
        <div class="content"><div class="box-content"><img width="660" height="256" src="http://somjedinecomam.sk/wp-content/uploads/2016/05/Teeth-Whitening-New-York-660x256.jpg" class="attachment-col4 wp-post-image" alt="Teeth-Whitening-New-York"></div></div>

        <div class="postmetadata">
            <hr>
            <div class="post-date">
                <strong>Posted on</strong>: máj 16th, 2016			 by 
                admin
            </div>
            <hr>
        </div>

        <div class="entry">
            <p><i>Určite to poznáte – „Osvedčené triky pre biely úsmev za pár dní“ alebo „Krásne zuby z&nbsp;pohodlia domova“ – časté titulky s&nbsp;bielením chrupu v&nbsp;hlavnej úlohe. Ak trápi odtieň vašich zubov aj vás, prípadne ste niekedy zvažovali jeho vylepšenie alebo ste ho dokonca podstúpili bez očakávaného výsledku, sú tieto riadky určené priamo vám. Faktom totiž je, že bielemu úsmevu sa venujeme čím ďalej tým viac pozornosti. A&nbsp;dôvody nemajú pôvod len v osobnom, ale často aj v&nbsp;profesionálnom živote. Áno, na zúbky sa vám dnes pozrú, aj keď je v&nbsp;hre prijatie do zamestnania či povýšenie.</i></p>
            <p><span id="more-5804"></span></p>
            <p><h3><b>Risk nie je vždy zisk</b></h3></p>
            <p>S&nbsp;narastajúcim trendom belších zubov prikráčali ruka v&nbsp;ruke rôzne techniky pre dosiahnutie želaných výsledkov. Ale čo si vybrať? Profesionálnu starostlivosť alebo domáce recepty, doma či v&nbsp;kresle odborníka? Ponuka je naozaj pestrá, ale aj tu platí staré známe „bezpečne ďalej zájdeš“. Napríklad bielenie domácimi technikami akými sú sóda bikarbóna, jahoda, citrón či pomarančová kôra môže poškodiť zubnú sklovinu a napáchať tak viac škody ako účinku. Omnoho bezpečnejšie sú procedúry konzultované so zubným lekárom. A&nbsp;tie potom pokojne zvládnete aj u&nbsp;vás doma.</p>
            <p>&nbsp;</p>
            <p><h3><b>Ako na to doma?</b></h3></p>
            <p>Ak sa chcete sa vyhnúť neobľúbeným sedeniam v&nbsp;ambulancii, máme pre vás tip. Aj keď úvodnú poradu s&nbsp;lekárom či dentálnou hygieničkou budete musieť absolvovať, samotné profesionálne bielenie bude len a&nbsp;len vo vašich rukách. Ak teda dostanete zelenú pre domáce zosvetlenie zubov, čiže váš chrup bude bez zubného kameňa, povlakov, kazov či zápalov, ostáva len disciplinovane dodržiavať inštrukcie pre aplikáciu odporúčaných prípravkov. Vďaka nim sa zmení sfarbenie pigmentov vo vnútri zubnej skloviny a zuby budú vyzerať belšie, zdravšie a čistejšie.</p>
            <p>&nbsp;</p>
            <p><h3><b>Jednoducho a&nbsp;pohodlne</b></h3></p>
            <p>Celú procedúru bielenia si nastavíte spoločne so zubným lekárom tak, ako to bude vyhovovať vám a&nbsp;stavu vašich zubov. Všetko pekne „na mieru“. Žiadne ordinačné hodiny, len vy, prípravok a&nbsp;aplikátor. V&nbsp;Interklinik sme stavili na bieliaci systém Polanight a&nbsp;Poladay – gély určené na denné a&nbsp;nočné ošetrenie. Gély sa jednoducho nanesú do aplikátora, vložia do úst a&nbsp;vy sa môžete na ceste za žiarivejším úsmevom venovať aj iným domácim aktivitám. Ako sa hovorí – „zabijete niekoľko múch jednou ranou“ a zaručene stihnete spraviť viac ako v&nbsp;kresle stomatológa.</p>
            <p>&nbsp;</p>
            <p><strong>Zopár faktov na záver</strong></p>
            <ul>
                <li>Zuby môžu byť počas aj po aplikácii bieliaceho prípravku citlivé – je to normálne</li>
                <li>Buďte trpezliví, nečakajte okamžité výsledky – viditeľná zmena odtieňu zubov sa dostaví za niekoľko dní, maximálny výsledok za asi 2-3 týždne</li>
                <li>Počas bielenia a&nbsp;tesne po jeho skončení sa pripravte sa na mierne obmedzenie v&nbsp;stravovaní – dočasnú „stopku“ v&nbsp;tomto období dostáva napríklad fajčenie, káva, čierny čaj či cvikla</li>
            </ul>
            <p>Čímkoľvek si nebudete istý, obráťte sa na lekára – poradí vám</p>
        </div>
    </div>
    <?php
}
?>