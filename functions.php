<?php
/* Start - Carousel */

function createCarousel() {
    ?>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php
            $files = array_slice(scandir("img/slider"), 2);  // array of filenames
            for ($i = 0; $i < count($files); $i++) {
                $first = ($i == 0) ? "active" : "";
                echo "<li data-target='#myCarousel' data-slide-to='$i' class=$first></li>";
            }
            ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php
            $i = 0;
            foreach ($files as $file) {
                $first = ($i == 0) ? "active" : "";
                echo "<div class='item $first'>
                        <img src='img/slider/$file'>
                     </div>";
                $i++;
            }
            ?>
        </div>

        <!-- Left and right controls -->
        <?php
        createButtons_NextPrev("left");
        createButtons_NextPrev("right");
        ?>
    </div>

    <!-- START OBRAZOK NAMIESTO SLIDERU -->
    <div class="container mobile-image hidden-lg hidden-md col-sm-12 col-xs-12">
        <div class="carousel-caption1">
            <p class="text-center mobile-p">Description</p>
        </div>
    </div>
    <!-- END OBRAZKU -->

    <?php
}

function createButtons_NextPrev($side) {
    $where = ($side == "right") ? "next" : "prev";
    echo "<a class='$side carousel-control' href='#myCarousel' role='button' data-slide='$where'>
            <span class='glyphicon glyphicon-chevron-$side' aria-hidden='true'></span>
            <span class='sr-only'>Previous</span>
        </a>";
}

/* End - Carousel */

/* Start - Content of the page */

function showContent() {
    // First time on the page - Show Last Updated Article
    if (!isset($_GET["section"]) && !isset($_GET['contentName'])) {
        $contentName = "letne_spomienky_na_tvari";
        setArticleBackground($contentName);
    } elseif (isset($_GET["contentName"])) {  // Show Article
        setArticleBackground($_GET["contentName"]);
        require_once "contents/articles/" . $_GET["contentName"] . ".php";
        echo "</div>";
    } elseif (isset($_GET["section"])) {  // Show List of Articles
        listOfArticles();
    }
}

function listOfArticles() {
    $section = $_GET["section"];
    foreach ($GLOBALS['sections'] as $name => $articles[0]) {
        if ($section == $name) {
            echo "<div class='content-wrap' style='background-image: url(" . $articles[0][1] . "); padding: 0 1%;'>"
            . "<div class='listOfArticlesContent'>";
            echo "<div class='banner banner-left'>
                        <a href='http://www.interklinik.sk/' target='_blank'>
                            <img src='img/banners/banner-left.png'> 
                        </a>
                    </div>";
            foreach ($articles[0][0] as $article) {
                include "contents/articles/" . $article . ".php";
            }
            echo "<div class='clear'></div>";
            echo "<div class='banner banner-right'>
                        <a href='http://www.interklinik.sk/' target='_blank'>
                            <img src='img/banners/banner-right.png'> 
                        </a>
                    </div>
                  </div>
                </div>";
        }
    }
}

function createArticleDescription($title, $picture, $posted, $description, $article) {
    ?>
    <div class="article-box">
        <div class="image-content">
            <a href="index.php?contentName=<?= $article ?>"><img src="<?= $picture ?>" /></a>
            <div class="actions">
                <div class="share-background"></div>
                <a href="http://www.facebook.com/sharer.php?u=http://www.clinicnew.sk/index.php?contentName=<?= $article ?>" target="_new">
                    <div class="share">
                        <img src="img/socials/facebook-share.png" />
                    </div>
                </a>
            </div>
        </div>

        <a href="index.php?contentName=<?= $article ?>"> <h2><?= $title ?></h2> </a>
        <hr>
        <div class="post-date">
            <strong>Posted on</strong>: <?= $posted ?>
        </div>
        <hr>
        <p><?= $description ?></p>
        <p><a href="index.php?contentName=<?= $article ?>">zobraziť celý článok...</a></p>
    </div>
    <?php
}

function setArticleBackground($contentName) {
    $contentBoxExists = false;
    foreach ($GLOBALS['sections'] as $name => $articles[0]) {
        if (in_array($contentName, $articles[0][0]) && !$contentBoxExists) {
            echo "<div class='content-wrap' style='background-image: url(" . $articles[0][1] . ");'>";
            require_once "contents/articles/$contentName.php";
            echo "</div>";
            $contentBoxExists = true;
        }
    }
}

/* End - Content of the page */