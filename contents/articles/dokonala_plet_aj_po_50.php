<?php
$title = "Dokonalá pleť aj po päťdesiatke. Rozlúštila som kód večnej mladosti!";
$picture = "http://somjedinecomam.sk/wp-content/uploads/2016/03/8-punkte-liquidlifting-freiburg-240x300.jpg";
$posted = "marec 8th, 2016 by admin";
$description = "Ako slobodná mamička som sa celý čas tak nejako prebíjala životom a vždy som myslela skôr na druhých ako na seba. Keď však deti odrástli a&nbsp;vyleteli z&nbsp;rodinného hniezda, zrazu som mala viac času. Konečne som sa začala o&nbsp;seba poriadne starať. Prihlásila som sa na jogu, zašla som si ku kamarátke na nechty a&nbsp;konečne som dočítala Evitovku, ktorá štyri roky na nočnom stolíku zapadala prachom. Dobehnúť zameškané sa však veľmi nedarilo mojej pleti.";

if (isset($_GET['section'])) {
    createArticleDescription($title, $picture, $posted, $description, "dokonala_plet_aj_po_50");
} else {
    ?>
    <div id="page" class="post">
        <h1 class="post-title">Dokonalá pleť aj po päťdesiatke. Rozlúštila som kód večnej mladosti!</h1>
        <div class="content"><div class="box-content"><img width="240" height="300" src="http://somjedinecomam.sk/wp-content/uploads/2016/03/8-punkte-liquidlifting-freiburg-240x300.jpg" class="attachment-col4 wp-post-image" alt="8-punkte-liquidlifting-freiburg-240x300"></div></div>        

        <div class="postmetadata">
            <hr>
            <div class="post-date"><strong>Posted on</strong>: marec 8th, 2016			 by 
                admin</div>
            <hr>
        </div>

        <div class="entry">
            <p><i>Ako slobodná mamička som sa celý čas tak nejako prebíjala životom a vždy som myslela skôr na druhých ako na seba. Keď však deti odrástli a&nbsp;vyleteli z&nbsp;rodinného hniezda, zrazu som mala viac času. Konečne som sa začala o&nbsp;seba poriadne starať. Prihlásila som sa na jogu, zašla som si ku kamarátke na nechty a&nbsp;konečne som dočítala Evitovku, ktorá štyri roky na nočnom stolíku zapadala prachom. Dobehnúť zameškané sa však veľmi nedarilo mojej pleti.</i></p>
            <p><span id="more-5766"></span></p>
            <blockquote><p><strong>„…vrásky mi pribúdali každým dňom“</strong></p></blockquote>
            <p>Predsa len, päť krížikov je už dosť. Čo by som len dala za drobné očné vrásky, ktoré ma zdobili v&nbsp;tridsaťpäťke. Ešte som ani nestihla dojesť posledný kúsok torty z&nbsp;oslavy päťdesiatky a&nbsp;moja pleť úplne stratila objem a&nbsp;jas. Mala som pocit, akoby sa z&nbsp;nej vytratila všetka voda a&nbsp;vrásky mi pribúdali každým dňom. Keďže som však nehodlala vyzerať ako turistická mapa Slovenského raja vo vrstevnicovom vyhotovení, snažila som sa neúprosný čas oklamať najrôznejšími spôsobmi.</p>
            <p>Okrem jogy som sa pravidelne začala venovať aj tvárovej gymnastike, dodržiavala som pitný režim a rozlúčila som sa tiež s&nbsp;fajčením. Do zaručene účinných krémov a&nbsp;pleťových sér som po tri mesiace vrážala polku výplaty. Dokonca mi na určitý čas prestali chodiť listové zásielky, keďže som jedného dňa otvorila poštárovi s&nbsp;najnovšou vychytávkou v&nbsp;oblasti pleťových masiek na tvári. „Mierne“ sa môjho zjavu vyľakal a&nbsp;nejaký ten piatok sa neukázal.</p>
            <p>Čo tam však po nedoručených listových zásielkach. Keby spomínaná maska, krémy, gymnastika a&nbsp;všetko možné aj nemožné, čo som skúšala aspoň pomohli. Nič však účinné nebolo a&nbsp;mne zostali len moje vrásky, zvädnutá pleť a&nbsp;oči pre plač. Jedného dňa som však na internete natrafila na blog Interklinik o&nbsp;nejakej americkej novinke a&nbsp;dosť ma to zaujalo. Išlo o&nbsp;výplňový materiál MD Codes, ktorý pomáha hollywoodskym celebritám na počkanie. Chcela som o&nbsp;tom vedieť viac, a&nbsp;tak som zašla za Slávkou Feix, ktorá tento zákrok v&nbsp;Interklinik robí.</p>
            <p>Pani doktorka mi všetko okolo tohto ošetrenia vysvetlila. Potešilo ma, že ku každej klientke pristupujú individuálne a&nbsp;všetko prispôsobujú proporciám jej tváre. Keď som okrem praktického prevedenia počula aj o&nbsp;jeho účinkoch, nebolo mi treba hovoriť viac. Celý zákrok bol takmer bezbolestný. Cítila som len jemné vpichy ihličiek, ktorými pani doktorka do špeciálnych bodov na mojej tvári dostala kyselinu hyalurónovú. Všetko to „všeho-všudy“ trvalo niečo okolo hodinky.</p>
            <blockquote><p><strong>„…po vráskach zrazu neostalo ani stopy“</strong></p></blockquote>
            <p>Vďaka kódu mladosti, ako ho Slávka Feix volá, sa moja pleť razom rozžiarila. Bola o&nbsp;poznanie mladšia a&nbsp;získala stratenú pružnosť. Zákrok mi navyše dodal objem presne tam, kde som ho potrebovala. Po vráskach zrazu neostalo ani stopy. Možno kde – tu len nejaká vráska radosti, keďže som sa na svoju „novú“ tvár nevedela vynadívať. Najlepšie bolo, že to celé vyzeralo úplne prirodzene. Keď po pár týždňoch prišla na návštevu moja staršia dcéra z&nbsp;Prahy, neverila vlastným očiam. Iba som sa na ňu usmiala a podala jej vizitku Slávky Feix. Ak niekedy bude chcieť urobiť radosť svojej svokre, určite sa blysne.</p>
        </div>
    </div>
    <?php
}
?>