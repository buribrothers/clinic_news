<?php
$title = "Letné spomienky na tvári";
$picture = "http://somjedinecomam.sk/wp-content/uploads/2016/08/summer-skin-700-620x620.jpg";
$posted = "august 30th, 2016 by admin";
$description = "Dni, kedy sme si vychutnávali dokonalý slnečný kúpeľ, sa nám pomaly krátia. Nie je však dôvod na smútok, snáď nás babie leto ešte aspoň trošku pošteklí po tvári a&nbsp;prehreje naše telo. Ak ste počas uplynulých dní pravidelne chytali bronz, nemali by ste zabudnúť na ošetrenie pleti po horúcej sezóne.";

if (isset($_GET['section'])) {
    createArticleDescription($title, $picture, $posted, $description, "letne_spomienky_na_tvari");
} else {
    ?>
    <div id="page" class="post">        
        <h1 class="post-title"><?php echo $title; ?></h1>
        <div class="content"><div class="box-content"><img width="620" height="620" src="<?php echo $picture; ?>" class="attachment-col4 wp-post-image" alt="letná pokožka Interklinik"></div></div>

        <div class="postmetadata">
            <hr>
    <!--                <strong>Category</strong>: <a href="http://somjedinecomam.sk/category/anti-aging/" rel="category tag" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">Anti-aging</a>, <a href="http://somjedinecomam.sk/category/estetika/" rel="category tag" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">Estetika</a>
            <div class="tags"><strong>Tags</strong>: <a href="http://somjedinecomam.sk/tag/antiaging-a-interklinik/" rel="tag" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">antiaging a interklinik</a>, <a href="http://somjedinecomam.sk/tag/dermatologia-interklinik/" rel="tag" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">dermatológia Interklinik</a>, <a href="http://somjedinecomam.sk/tag/oxygeneo/" rel="tag" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">oxygeneo</a>, <a href="http://somjedinecomam.sk/tag/peeling/" rel="tag" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">peeling</a> </div>
            --><div class="post-date">
                <strong>Posted on</strong>: <?php echo $posted; ?>
            </div>
            <hr>
        </div>

        <div class="entry">
            <p><i>Dni, kedy sme si vychutnávali dokonalý slnečný kúpeľ, sa nám pomaly krátia. Nie je však dôvod na smútok, snáď nás babie leto ešte aspoň trošku pošteklí po tvári a&nbsp;prehreje naše telo. Ak ste počas uplynulých dní pravidelne chytali bronz, nemali by ste zabudnúť na ošetrenie pleti po horúcej sezóne. Nie je nič nezvyčajné, ak sa na vašej pleti objavili pigmentové škvrny, alebo sa vaša pleť napriek dôkladnému ošetrovaniu vysokým UV faktorom vysušila. Doprajte jej teda malú rekonvalescenciu.</i><span id="more-5825"></span></p>
            <p><h3><b>Letná obeta</b></h3></p>
            <p>Kým ste vy dovolenkovali, vaša pleť si to „odmakala“. Áno, práve pokožka je počas letných mesiacov zaťažovaná niekoľkonásobne viac, než inokedy počas roka. Ak ju chcete odmeniť, doprajte jej chemický peeling a&nbsp;OxyGeneo. Tieto dve procedúry šetrne eliminujú následky leta. Prvá zatočí s&nbsp;pigmentovými škvrnami, druhá zase podporí hydratáciu pleti a&nbsp;tvorbu kolagénu.</p>
            <p><h3><b>Peeling kyselinkou</b></h3></p>
            <p>Toto ošetrenie&nbsp; si netreba predstavovať ako v&nbsp;hororovom filme. Chemický peeling sa používa v&nbsp;dermatológii už od 40-tych rokov minulého storočia. Na tvár sa po očistení pleti štetcom nanesie &nbsp;roztok kyseliny trichlóroctovej spolu s rastovými a regeneračnými faktormi na ošetrovanú oblasť maximálne v troch vrstvách. Po zaschnutí poslednej vrstvy sa jemne umyje vodou a ošetrí protizápalovou maskou a ošetrí krémom. Výsledkom je zjemnenie príznakov starnutia pleti, fľakov či odstránenie suchých šupiniek. Na toto ošetrenie si však zájdite až vtedy, keď už vašu pokožku nevystavujete slnku a&nbsp;nie je spálená.</p>
            <p><h3><b>Kyslíkový zázrak</b></h3></p>
            <p>Ďalšou osvedčenou procedúrou, ktorá vám odoberie pár rokov z&nbsp;rodného listu je OxyGeneo. OxyGeneo pôsobí na pleť hĺbkovo, a&nbsp;tak ju dokáže okysličiť priamo zvnútra, vyživiť a&nbsp;zabrániť jej starnutiu. Toto ošetrenie je navyše vhodné pre akýkoľvek typ pleti, a&nbsp;tak ho môže vyskúšať každá dáma bez ohľadu na citlivosť jej pokožky. Celá procedúra trvá približne 40 minút a&nbsp;pozostáva z&nbsp;troch krokov. V&nbsp;prvom vám z&nbsp;pleti odstránime odumreté bunky, vďaka čomu pripravíme „pôdu“ na nasatie takzvanej infúzie v&nbsp;podobe živín a&nbsp;aktívnych látok. V&nbsp;tretej fáze si užijete príjemné praskanie kyslíkovej šumienky na tvári, ktorá zvyšuje prietok krvi a&nbsp;metabolizmus. Pokožka je po ošetrení viditeľne krajšia, pevnejšia a&nbsp;zjednotená a&nbsp;vy sebavedomejšia a&nbsp;spokojná.</p>
            <p><a href="http://www.interklinik.sk" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">www.interklinik.sk</a></p>
        </div>
    </div>
    <?php
}
?>