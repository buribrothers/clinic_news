<?php
$title = "ZDRAVÉ SPRÁVY Pre zdravie bábätka – zvýšte príjem jódu";
$picture = "http://somjedinecomam.sk/wp-content/uploads/2014/12/Tehotnosť-22-660x434.jpg";
$posted = "december 17th, 2014 by admin";
$description = "Jód je pre zdravý vývoj a fungovanie ľudského organizmu mimoriadne dôležitý. Štítna žľaza potrebuje jód k produkcii dôležitých hormónov zodpovedných za významné funkcie nášho organizmu. Pri jeho nedostatku môžu nastať poruchy funkcie štítnej žľazy, ktoré vyvolávajú ďalšie zdravotné problémy.";

if (isset($_GET['section'])) {
    createArticleDescription($title, $picture, $posted, $description, "zdrave_spravy_pre_zdravie_babetka");
} else {
    ?>
    <div id="page" class="post">
        <h1 class="post-title">ZDRAVÉ SPRÁVY Pre zdravie bábätka – zvýšte príjem jódu</h1>
        <div class="content"><div class="box-content"><img width="660" height="434" src="http://somjedinecomam.sk/wp-content/uploads/2014/12/Tehotnosť-22-660x434.jpg" class="attachment-col4 wp-post-image" alt="tehotenstvo-ilustr"></div></div>        

        <div class="postmetadata">
            <hr>
            <div class="post-date"><strong>Posted on</strong>: december 17th, 2014			 by 
                admin</div>
            <hr>
        </div>

        <div class="entry">
            <p><i>Jód je pre zdravý vývoj a fungovanie ľudského organizmu mimoriadne dôležitý. Štítna žľaza potrebuje jód k produkcii dôležitých hormónov zodpovedných za významné funkcie nášho organizmu. Pri jeho nedostatku môžu nastať poruchy funkcie štítnej žľazy, ktoré vyvolávajú ďalšie zdravotné problémy.</i>&nbsp;<span id="more-5236"></span></p>
            <p>Osobitnú skupinu tvoria <strong>tehotné ženy.</strong> Jód zohráva svoju mimoriadne dôležitú úlohu už počas tehotenstva, teda od samého počiatku vývinu plodu, pričom dôsledky nedostatku jódu v tomto období sa už nedajú nikdy zvrátiť.</p>
            <p>Z toho vyplýva, že tehotné ženy potrebujú <strong>zvýšené množstvo jódu</strong>, teda viac, ako je odporúčaná denná dávka – 150 mikrogramov pre dospelého človeka.</p>
            <p>Tehotná žena musí zásobovať jódom nielen seba ale aj vyvíjajúci sa plod dieťatka a jeho vlastnú štítnu žľazu. Tá plní svoju funkciu už od 12. týždňa tehotenstva. Práve jód zohráva kľúčovú úlohu najmä v súvislosti s vývinom mozgu a následne jeho správnym fungovaním.</p>
            <p>Svetová zdravotnícka organizácia WHO upozorňuje, že nedostatok jódu v tehotenstve môže spôsobiť veľmi vážne komplikácie:<strong><br>
                </strong></p>
            <ul>
                <li>potrat</li>
                <li>vrodené anomálie</li>
                <li>endemický kreténizmus</li>
                <li>mutizmus (neurologická porucha reči)</li>
                <li>mentálnu retardáciu</li>
                <li>zvýšenie perinatálnej mortality, (popôrodnej úmrtnosti)</li>
            </ul>
            <p>Slovenskí odborníci poukazujú na mimoriadne dôležité odporúčania Svetovej zdravotníckej organizácie <a href="http://www.who.int/en/" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">WHO</a> a organizácie pre kontrolu porúch spôsobených nedostatkom jódu <a href="http://www.ign.org/p142000253.html" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">ICCIDD</a> (The International Council for Control of Iodine Deficiency Disorders), ktoré odporúčajú tehotným a dojčiacim ženám <strong>zvýšenú dennú dávku jódu</strong> – až 250 mikrogramov denne, čo je takmer dvojnásobok odporúčanej dennej dávke pre dospelého jedinca.<strong>&nbsp;</strong></p>
            <p>ICCIDD vyberá, ako alternatívu na riešenie jódového deficitu u tehotných žien, (ale aj pre iné skupiny populácie) výživový doplnok <a href="http://www.jodis.sk/" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">koncentrát JODIS</a>.</p>
            <p>Koncentrát JODIS – je prírodná voda s nízkym obsahom minerálov, obohatená vysokým obsahom zlúčenín jódu, presne podľa požiadaviek a noriem EÚ. Mnohí slovenskí gynekológovia JODIS koncentrát odporúčajú najmä z dôvodu, že <strong>nemá chuť ani zápach,</strong> preto je <strong>ľahko prijateľný tehotnými ženami</strong>. Nakoľko organizmus nadbytočný jód vylúči z tela matky von, <strong>nie je možné sa ním predávkovať.</strong></p>
            <p>&nbsp;</p>
            <blockquote><p><a href="http://www.interklinik.sk/gynekologia/prenatalna-diagnostika-morfologicky-ultrazvuk" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">Komplexná príprava</a> budúcej mamičky na pôrod (psychická aj fyzická) – má za úlohu nielen informovať o zásadách správnej životosprávy, ale tiež zbaviť rodičku strachu a obáv, pretože pôrod je bežná fyziologická záležitosť. Psychofyzická príprava je nevyhnutným základom všetkých metód, ktoré podporujú a uľahčujú pôrod. – <a href="http://www.interklinik.sk/gynekologia/mudr-dagmar-gavornikova" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">MUDr.Dagmar Gavorníková&nbsp;</a></p></blockquote>
            <h4>OBJEDNAJTE SA TERAZ&nbsp; <span style="color: #ff0000;">0905 48 08 48</span></h4>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>Zdroj: wn-pr, interklinik</p>
            <p>Prečítajte si tiež: &nbsp;<a href="http://somjedinecomam.sk/2014/04/psychofyzicka-priprava-na-porod/" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">Príprava na pôrod</a>;&nbsp;<a href="http://somjedinecomam.sk/2014/05/gynekolog-nevolnost-v-tehotenstve/" data-slimstat-clicked="false" data-slimstat-type="2" data-slimstat-tracking="false" data-slimstat-async="false" data-slimstat-callback="false">Nevoľnosť v tehotenstve;</a></p>
            <p><b>Túžite po pestrejších informáciách?</b></p>
            <p>Spojte sa s nami:</p>
            <ul>
                <li>na&nbsp;<a href="http://www.interklinik.sk/" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">Interklinik.sk</a><b>&nbsp;</b>nájdete všetko o nás</li>
                <li>na<a href="http://www.facebook.com/pages/Interklinik-centrum-zdravia-a-krasy/182030630479" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">&nbsp;Facebooku</a>&nbsp;je všetko pre fanúšikov</li>
                <li>na&nbsp;<a href="http://www.flickr.com/photos/sijedinecomas/" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">Flickr&nbsp;</a>na vás prehovoria naše fotky</li>
                <li>na&nbsp;<a href="http://www.linkedin.com/company/interklinik" data-slimstat-clicked="false" data-slimstat-type="0" data-slimstat-tracking="true" data-slimstat-async="false" data-slimstat-callback="true">LindkedIn</a>&nbsp;získajte profesionálne informácie.</li>
            </ul>
        </div>
    </div>
    <?php
}
?>