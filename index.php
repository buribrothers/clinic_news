<?php
require_once 'config.php';
require_once 'functions.php';
?>

<!DOCTYPE html>
<html lang="cs-cz">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Clinic News</title>

        <!-- CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
              integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">-->
        <!--        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>-->
        <link rel="stylesheet" href="css/custom.css" type="text/css">
        <link rel="stylesheet" href="css/animate.css" type="text/css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">        

    </head>
    <body>
        <!-- Start - Logo -->
        <h1 class="text-center" style="margin-bottom: 50px;"><a href="index.php"><b>Logo Clinic News</b></a></h1>
        <!-- End - Logo -->

        <!-- Start - SLIDER -->
        <?= createCarousel(); ?>
        <!-- End - Slider -->

        <!-- ARTICLE -->
        <?= showContent(); ?>
        <!-- END OF ARTICLE -->

        <!-- SECTIONS -->
        <section id="join" class="container-fluid">
            <div class="row text-center">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 join1 fadeInLeft">
                    <a href="index.php?section=anti-aging"><span class="join-info">ANTI-AGING</span></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 join3  fadeInRight">
                    <a href="index.php?section=dentalna-starostlivost"><span class="join-info">DENTÁLNA STAROSTLIVOSŤ</span></a>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 join2 fadeInLeft">
                    <a href="index.php?section=poradna-pre-mamicky"><span class="join-info">PORADŇA PRE MAMIČKY</span></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 join4 fadeInRight">
                    <a href="index.php?section=estetika"><span class="join-info">ESTETIKA</span></a>
                </div>
            </div>
        </section>
        <!-- END OF THE SECTIONS -->

        <!-- START OF THE FOOTER -->
        <section id="footer" class="">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    <h2>Follow us</h2>
                    <table align="center">
                        <tr>
                            <td>
                                <img id="my-img" src="img/socials/facebook-off.png" onmouseover="hover(this, 'fb');" onmouseout="unhover(this, 'fb');" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img id="my-img" src="img/socials/instagram-off.png" onmouseover="hover(this, 'insta');" onmouseout="unhover(this, 'insta');" />
                            </td>
                        </tr>
                        <tr>
                            <td onclick="showContact()" class="linkToRedaction">Kontakt do Redakcie</td>
                        </tr>
                        <tr>
                            <td class="kontakt">
                                <b>Adresa:</b> Seberíniho 9,
                                                Ružinov,
                                                821 05 Bratislava<br />
                                <b>Telefón:</b> +421 918 860 001<br />
                                <b>Email:</b> <a href="mailto:info@clinicnew.sk">info@clinicnew.sk</a><br />
                            </td>
                        </tr>
                        <tr>
                            <td>&copy; Clinic News - 2016</td>
                        </tr>
                    </table>
                </div>
            </div>
        </section>
        <!-- END OF THE FOOTER -->

        <!-- Javascript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!--        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>-->
        <script src="js/master.js"></script>
    </body>
</html>
