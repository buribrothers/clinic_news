<?php
$title = "Povädnutý dekolt opäť v pozore: krásna aj po pôrode";
$picture = "http://somjedinecomam.sk/wp-content/uploads/2016/02/the-natural-boob-job-fat-transfer-breast-augmentation-730952_w1020h450c1cx506cy248-660x291.jpg";
$posted = "február 2nd, 2016 by admin";
$description = "Dekolt je miesto, kam zvedavé mužské oči vždy rady skĺznu pohľadom a&nbsp;pokochajú sa jeho obsahom. V&nbsp;tom mojom bolo toho vždy na kochanie viac ako dosť, koniec koncov na tie údajne najkrajšie prsia v&nbsp;okrese aj kraji som vraj zbalila aj manžela. Po pôrode a&nbsp;kojení to však s&nbsp;nimi šlo dolu vodou. Odmenou za každodenné mliečne menu pre nášho juniora mi boli mínus dve čísla a&nbsp;povädnuté prsia, ktoré nedali dopustiť na zákon gravitácie.";

if (isset($_GET['section'])) {
    createArticleDescription($title, $picture, $posted, $description, "povadnuty_dekolt_opat_v_pozore");
} else {
    ?>
    <div id="page" class="post">
        <h1 class="post-title">Povädnutý dekolt opäť v pozore: krásna aj po pôrode</h1>
        <div class="content"><div class="box-content"><img width="660" height="291" src="http://somjedinecomam.sk/wp-content/uploads/2016/02/the-natural-boob-job-fat-transfer-breast-augmentation-730952_w1020h450c1cx506cy248-660x291.jpg" class="attachment-col4 wp-post-image" alt="augmentácia_poprsia"></div></div>        

        <div class="postmetadata">
            <hr>
            <div class="post-date"><strong>Posted on</strong>: február 2nd, 2016			 by 
                admin</div>
            <hr>
        </div>

        <div class="entry">
            <p><i>Dekolt je miesto, kam zvedavé mužské oči vždy rady skĺznu pohľadom a&nbsp;pokochajú sa jeho obsahom. V&nbsp;tom mojom bolo toho vždy na kochanie viac ako dosť, koniec koncov na tie údajne najkrajšie prsia v&nbsp;okrese aj kraji som vraj zbalila aj manžela. Po pôrode a&nbsp;kojení to však s&nbsp;nimi šlo dolu vodou. Odmenou za každodenné mliečne menu pre nášho juniora mi boli mínus dve čísla a&nbsp;povädnuté prsia, ktoré nedali dopustiť na zákon gravitácie.</i><span id="more-5739"></span></p>
            <blockquote><p>„…ani som si nevšimla, ako sa moje prsia časom zmenili“</p></blockquote>
            <p>Newton by bol síce na ne hrdý, moja spokojnosť však už bola menšia. Kojila som dosť dlho a&nbsp;priznám sa, hneď som si ani nevšimla, ako sa moje prsia časom zmenili. Jedného dňa, bolo to už po tom, ako som pre nášho malého svoju pojazdnú reštauráciu nadobro zavrela, som si šla kúpiť novú podprsenku. Sánka šla dole v&nbsp;momente, keď som si vyskúšala svoju tradičnú D-éčkovú a&nbsp;pŕs zrazu nikde. Jednoducho zmizli v&nbsp;jej útrobách a&nbsp;ja som sa musela uspokojiť so slabým B-éčkom.</p>
            <p>Mala som z&nbsp;toho hlavu v&nbsp;smútku. Vyskúšala som snáď všetky možné aj nemožné internetové tipy. Ani prsná gymnastika, ani hektolitre potu v&nbsp;posilňovni, zázračné tabletky či zaručene účinné gély však nespravili zázraky.</p>
            <blockquote><p>„…nechala som si od nej poradiť a&nbsp;vybrala som sa do Interklinik“</p></blockquote>
            <p>A&nbsp;tak som si začala s&nbsp;mojimi pobožnými dvojkami a&nbsp;sebavedomím na bode takom nízkom, že ani januárové mínusy pána Iľka sa na to nechytali, študovať diskusné fóra. Keď som potom ešte dala pár kávičiek s&nbsp;kamarátkou, ktorá si minulý rok dopriala isté vylepšenie, bola som rozhodnutá. Nebudem už čakať ani chvíľu, idem na plastiku! Nechala som si od nej poradiť a&nbsp;vybrala som sa do Interklinik.</p>
            <p>Na túto kliniku totiž nedala dopustiť a&nbsp;mala pravdu. Ja som Interklinik poznala len cez svojho gynekológa, som totiž ich dlhoročnou pacientkou. Nikdy som sa však nezaujímala o&nbsp;ďalšie veci, ktoré na klinike robia. Ujal sa ma doktor Rácz, ktorý je obrovský profesionál. Spoločne sme vybrali to najvhodnejšie pre môj nový dekolt. Urobil mi tiež všetky potrebné predoperačné vyšetrenia, dokonca aj sono. Nemusela som tak behať po ďalších vyšetreniach mimo kliniky.</p>
            <p>Keď už malo prísť na lámanie chleba, dostala som z&nbsp;operácie zrazu trochu strach. Ten bol však úplne zbytočný. Po dvoch hodinách som sa zobudila svieža ako rybička, a&nbsp;síce som mala ešte menšie bolesti, tie sa po pár dňoch úplne vytratili. Potom som sa v&nbsp;Interklinik zastavila už len na vyberanie stehov a&nbsp;samozrejme potriasť pánovi Ráczovi jeho zlatými ručičkami. Tie totiž dokázali spraviť s&nbsp;mojim dekoltom zázraky. Opäť sa v&nbsp;ňom skrývali najkrajšie prsia v&nbsp;okrese a&nbsp;možno aj v&nbsp;kraji. A&nbsp;aby toho nebolo málo, ako bonus som k&nbsp;novým prsiam dostala aj poukaz na korekciu viečok. Mali akurát nejakú špeciálnu akciu. Takže najbližšie pôjdem do Interkliniku po nový žiarivý pohľad. Už sa neviem dočkať.</p>
            <p>&nbsp;</p>
        </div>

    </div>
    <?php
}
?>