<?php
$title = "Výživa pri tehotenskej cukrovke";
$picture = "http://somjedinecomam.sk/wp-content/uploads/2016/07/18513-tehotenstvo-materstvo-vyziva-ovocie-tehotna-cukrovka-clanok.jpg";
$posted = "júl 11th, 2016 by admin";
$description = "Tehotenská cukrovka (gestačný diabetes mellitus) je špeciálny typ cukrovky, ktorý sa objavuje počas tehotenstva a po pôrode spontánne ustúpi. Jej liečba spočíva najmä v úprave výživy a životosprávy. V dnešnej dobe sa čoraz viac upúšťa od názvu diabetická diéta, pretože súčasné výživové odporúčania pre diabetikov sa iba mierne líšia od zásad správnej výživy, ktoré by mal dodržiavať každý, kto chce byť zdravý. Cieľom výživových odporúčaní pri diabete je udržiavanie hladiny cukru v krvi v normálnych hodnotách.";

if (isset($_GET['section'])) {
    createArticleDescription($title, $picture, $posted, $description, "vyziva_pri_tehotenskej_cukrovke");
} else {
    ?>
    <div id="page" class="post">        
        <h1 class="post-title">Výživa pri tehotenskej cukrovke</h1>
        <div class="content"><div class="box-content"><img width="470" height="353" src="http://somjedinecomam.sk/wp-content/uploads/2016/07/18513-tehotenstvo-materstvo-vyziva-ovocie-tehotna-cukrovka-clanok.jpg" class="attachment-col4 wp-post-image" alt="18513-tehotenstvo-materstvo-vyziva-ovocie-tehotna-cukrovka-clanok"></div></div>

        <div class="postmetadata">
            <hr>
            <div class="post-date"><strong>Posted on</strong>: júl 11th, 2016			 by 
                admin 
            </div>
            <hr>
        </div>

        <div class="entry">
            <p><i>Tehotenská cukrovka (gestačný diabetes mellitus) je špeciálny typ cukrovky, ktorý sa objavuje počas tehotenstva a po pôrode spontánne ustúpi. Jej liečba spočíva najmä v úprave výživy a životosprávy.</i><br>
                V dnešnej dobe sa čoraz viac upúšťa od názvu diabetická diéta, pretože súčasné výživové odporúčania pre diabetikov sa iba mierne líšia od zásad správnej výživy, ktoré by mal dodržiavať každý, kto chce byť zdravý. Cieľom výživových odporúčaní pri diabete je udržiavanie hladiny cukru v krvi v normálnych hodnotách.<span id="more-5822"></span><br>
            <p><h3><b>Vhodné a nevhodné cukry</b></h3></p>
            Najväčší nárast hladiny cukru v krvi spôsobujú jednoduché sacharidy – cukry, ktoré rozpoznáme podľa ich sladkej chuti. Kryštálový cukor, sladké druhy pečiva, zákusky, čokoláda, sladké pokrmy, sladené nápoje, kompóty, med, zmrzlina, kandizované ovocie a iné sladkosti patria pri tehotenskej cukrovke medzi nevhodné potraviny. Ich príjem sa odporúča výrazne obmedziť.<br>
            Ak nás chuť na sladké neúprosne prenasleduje, lepšie je drobnú sladkosť skonzumovať ako súčasť väčšieho pokrmu, napríklad obeda alebo večere. Hladina cukru sa nezvýši v takej miere, ako keby sa maškrta konzumovala samostatne. Treba pamätať, že nemalé množstvo jednoduchých cukrov je aj v ovocí, ktoré tiež nemožno konzumovať v ľubovoľnom množstve.<br>
            Organizmus však pre svoju riadnu funkciu potrebuje aj cukry. Pri diabete sa zaraďujú do stravy najmä zložité cukry – polysacharidy, ktoré nezvyšujú hladinu glukózy v krvi tak prudko ako jednoduché cukry. Podobne, ako u zdravých ľudí, aj u diabetikov tvoria polysacharidy hlavnú zložku stravy – hradiť majú 50-55 % z celkového príjmu energie. Ich zdrojom je chlieb, pečivo, ryža, pohánka, cestoviny a ďalšie výrobky z obilnín, strukoviny, kukurica, zemiaky a zelenina.<br>
            Používanie umelých sladidiel (napr. sacharín, aspartam, acesulfam, cyklamát) je počas tehotenstva nevhodné.</p>
            <p><h3><b>Sacharidové výmenné jednotky</b></h3></p>
            Jednoduchšie sledovanie príjmu cukrov umožní letáčik, či brožúrka s údajmi o sacharidových výmenných jednotkách. Jedna sacharidová jednotka je množstvo potraviny v gramoch, ktoré obsahuje 10 g sacharidov, čo je napríklad pol grahamového rožka, alebo 200 g jogurtu, či jeden pomaranč.<br>
            Pri tehotenskom diabete je povolených približne 250 gramov sacharidov na deň (225-275), to zodpovedá 25 sacharidovým jednotkám. V rámci tohto počtu sacharidových jednotiek si budúca mamička môže do jedálnička pestro kombinovať rôzne potraviny podľa vlastnej chuti a stravovacích zvyklostí.<br>
            <p><h3><b>Glykemický index</b></h3></p>
            Pri rozhodovaní, či je potravina vhodná alebo nie, napomôže aj údaj o jej glykemickom indexe. Informuje, v akej miere daná potravina zvyšuje hladinu cukru v krvi. Potraviny s vysokým glykemických indexom spôsobujú veľký nárast glykémie, preto sa diabetikom neodporúčajú. Naopak, základom stravy by mali byť potraviny s nízkym glykemickým indexom (pod 50).</p>
            <p><h3><b>Pravidelná a pestrá strava</b></h3></p>
            Pri tehotenskej cukrovke je dôležité dodržiavať pravidelnosť v stravovaní, ktorou možno predísť zbytočnému a nežiaducemu kolísaniu hladín cukru v krvi. Budúca mamička má jedávať 5-6 x denne, približne v 3 hodinových odstupoch.<br>
            Odporúčajú sa tri väčšie pokrmy a medzi nimi 2-3 ľahšie, menšie jedlá. Správne je dennú dávku cukrov rovnomerne rozdeliť v rámci jednotlivých jedál. Predpokladom dostatočného príjmu všetkých živín je rozmanitá strava a pestré obmieňanie rozličných potravín.</p>
            <p><h3><b>Pomôže dostatok vlákniny</b></h3></p>
            Prospešnou súčasťou výživy pri diabete je vláknina, ktorá spomaľuje vstrebávanie cukrov z tráviacej sústavy do krvi. Zabraňuje tým nadmerne rýchlemu vzostupu hladiny cukru.<br>
            Príjem vlákniny možno zvýšiť uprednostňovaním celozrnných obilninových výrobkov pred bielymi. Jej bohatým zdrojom je zelenina a ovocie, ktoré by mali byť súčasťou každého denného pokrmu. Nachádza sa i v strukovinách, ovsených vločkách, alebo semienkach olejnatých plodín.</p>
            <p><h3><b>Nápoje</b></h3></p>
            Pri výbere nápojov nezabúdajme, že veľké množstvo cukru obsahujú aj sladené nápoje a džúsy. Hladinu cukru zvyšuje aj laktóza – mliečny cukor, preto mlieko a mliečne nápoje sa tiež započítava do množstva skonzumovaných sacharidových jednotiek.</p>
            <p><h3><b>Strážte si hmotnosť</b></h3></p>
            Hladinu cukru v krvi nepriaznivo ovplyvňujú kilá navyše. V tehotenstve je priberanie samozrejmosťou, ak ale kilá pribúdajú rýchlejšie ako by mali, môžu sa podieľať na vzniku tehotenskej cukrovky.<br>
            Tehotná žena musí jedávať za dvoch, neznamená to však, že by mala konzumovať dvojnásobné porcie jedla. Denná potreba kalórii sa v tehotenstve zvyšuje približne iba o 300 kcal.<br>
            V prvých troch mesiacoch gravidity by mal byť prírastok na hmotnosti asi 2 kg, neskôr priemerne 350 g za týždeň. Budúca mamička by počas tehotenstva mala celkovo pribrať 8-12 kg, v prípade obezity iba 7 kg.</p>
            <p><h3><b>Pravidelný pohyb</b></h3></p>
            prospieva látkovej premene cukrov a je aj účinnou prevenciou nadmerného priberania. Šport, fyzická aktivita v miernom tempe by sa mala by sa stať počas tehotenstva každodennou súčasťou denného režimu.<br>
            Podávanie inzulínu<br>
            Ak zmena výživy a životosprávy nepostačí a hladina cukru sa neupraví, potrebné je aj podávanie inzulínu. Neliečená cukrovka v tehotenstve môže spôsobiť komplikácie pri pôrode a je rizikom najmä pre dieťa. Tehotenskej cukrovke treba venovať patričnú pozornosť, dodržiavať vhodný stravovací režim a ďalšie odporúčania lekára.</p>
        </div>
    </div>
    <?php
}
?>