<?php
// ------- Parameters Definition ------ //
$anti_aging             = ["letne_spomienky_na_tvari", "dokonala_plet_aj_po_50", "bielim_bielis_bielime"];
$dentalna_starostlivost = ["bielim_bielis_bielime", "dentalna_hygiena"];
$poradna_pre_mamicky    = ["vyziva_pri_tehotenskej_cukrovke", "zdrave_spravy_pre_zdravie_babetka", "povadnuty_dekolt_opat_v_pozore"];
$estetika               = ["to_najlepsie_ako_sa_zbavit_brucha", "ako_na_rozsirene_cievky", "povadnuty_dekolt_opat_v_pozore", "letne_spomienky_na_tvari"];

$sections = ["anti-aging"               => [$anti_aging, "https://static.pexels.com/photos/33109/fall-autumn-red-season.jpg"],
            "dentalna-starostlivost"    => [$dentalna_starostlivost, "img/lemon-ice-mint-4k-wallpaper-1920x1080.jpg"],
            "poradna-pre-mamicky"       => [$poradna_pre_mamicky, "https://static.pexels.com/photos/2698/dawn-landscape-mountains-nature.jpg"],
            "estetika"                  => [$estetika, "https://static.pexels.com/photos/108055/pexels-photo-108055.jpeg"]];

?>