function hover(element, type) {
    if (type === 'fb') {
        element.setAttribute('src', 'img/socials/facebook-on.png');
    } else {
        element.setAttribute('src', 'img/socials/instagram-on.png');
    }
}
function unhover(element, type) {
    if (type === 'fb') {
        element.setAttribute('src', 'img/socials/facebook-off.png');
    } else {
        element.setAttribute('src', 'img/socials/instagram-off.png');
    }
}

function showContact() {
    var r = $('#footer .kontakt');
    if (r.attr("style") == "display: none;" || r.attr("style") == null) {
        r.fadeIn();
    } else if (r.attr("style") == "display: table-cell;") {
        r.fadeOut();
    }

}

$(document).ready(function() {
    
$(".article-box").hover(
    function () {
        console.log("handler in");
        $(".actions", this).fadeIn("fast");
    }, function () {
        console.log("handler out");
        $(".actions", this).fadeOut();
    }
);
});